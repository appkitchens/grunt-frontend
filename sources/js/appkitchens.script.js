/*!
 * AppKitchens Script
 *
 * Copyright 2014 AppKitchens. All rights reserved.
 *
 * Author: Faris
 * Founder of AppKitchens
 * Follow @farishjazz @AppKitchens
 * 
 */

var mapObj = {
	map: undefined,
	lat: 0,
	lng: 0,
	markerMap: []
};

(function($){
	"use strict"

	$(function(){
		$(window).on('load', function(){
			$('#loader').fadeOut(function(){
				$('#loader').remove();
			})
		});
	});

	$(document).ready(function() {
		$('.link-contact-us').click(function(){
			$('#maps').slideToggle(function(){
				var center = mapObj.map.getCenter();
	    		google.maps.event.trigger(mapObj.map, "resize");
	    		mapObj.map.setCenter(center);
			});
		});

		google.maps.event.addDomListener(window, 'load', function(){
			mapObj.map = new google.maps.Map(document.getElementById('maps'), { 
				zoom: 16, center: new google.maps.LatLng(mapObj.lat, mapObj.lng) 
			});

			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(mapObj.lat, mapObj.lng),
				map: mapObj.map
			});

			mapObj.markerMap.push(marker);
		});

		var stickyNavTop = $('#sticky-bar').offset().top;  
	    var stickyNav = function(){  
	        var scrollTop = $(window).scrollTop();  
	        if (scrollTop > stickyNavTop) { $('#sticky-bar').addClass('sticky'); } 
	        else { $('#sticky-bar').removeClass('sticky'); } 
	    };  
	    stickyNav();  
	    $(window).scroll(function() { stickyNav(); });  
	})
})(jQuery);