APPKITCHENS FRONTEND FRAMEWORK
USING GRUNT

Prerequisites:
---------------------
- Make sure you already have nodejs, grunt and also ruby
- After that please execute following command:
	- npm install grunt
	- npm install -g grunt-cli
	- npm install grunt --save-dev
	- npm install

Commands
---------------------
- grunt => to compile and build
- grunt server => for testing in browser, default port 9000

Folder Structure
---------------------
- sources
	- this folder contain the main source of the project
- sources/templates
	- master file for initializing head & footer
- dist
	- this folder will be generated after compiling
- assets
	- temporary folder before finishing the compiler 