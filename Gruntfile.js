path = require('path');
module.exports = function(grunt) {

  var mountFolder = function(connect, dir) {
    return connect.static(require('path').resolve(dir));
  };
  // configure project
  grunt.initConfig({
    compass: {
      dist: {
        options: {
          config : "config.rb",
          sassDir: 'sources/css',
          javascriptsDir: 'sources/js',
          outputStyle: 'compact', // nested, expanded, compact, compressed.
          noLineComments: false,
          environment: "production", // or production
          cssDir: 'assets/css',
          imagesDir: 'assets/img',
          watch: false
        }
      }
    },
    prettify: {
      options: {
        indent: 1,
        indent_char: "  ",
        condense: true,
        brace_style: "expand",
        padcomments: true,
        indent_scripts: 'separate',
        preserve_newlines: true,
        unformatted: [
          "pre", "a", "span"
        ]
      },
      all: {
          expand: true, 
          cwd: '', 
          ext: '.html',
          src: ['*.html'],
          dest: 'dist'
      }
    },
    jade: {
      compile: {
        options: {
          pretty: true
        },
        files: [{
          expand: true,
          cwd: 'sources',
          src: ['*.jade'],
          ext: '.html',
          dest: 'dist'
        }]
      }
    },
    watch: {
      options: {
        livereload: true,
        nospawn: true
      },
      css: {
        files: ['sources/css/**/*'],
        tasks: ['compass']
      },
      js: {
        files: ['sources/js/**/*'],
        tasks: ['prettify']
      },
      templates: {
        files: ["sources/*.jade", "sources/templates/*.jade"],
        tasks: ['jade', 'prettify'],
        options: {
          nospawn: true
        }
      }
    },
    connect: {
      server: {
        options: {
          livereload: true,
          port: 9000,
          middleware: function(connect) {
            return [
              require('connect-livereload')(),
              mountFolder(connect, 'dist')
            ];
          }
        }

      }
    },
    uglify: {
      options: {
        mangle: true
      },
      dist: {
        files: {
          'dist/assets/js/app.js': [ "assets/js/*" ]
        }
      }
    },
    open: {
      server: {
        url: 'http://localhost:9000/'
      }
    },
    copy: {
      dist: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: "assets",
            dest: "dist/assets",
            src: [
              "css/*"
            ]
          },
          {
            expand: true,
            dot: true,
            cwd: "sources",
            dest: "dist/assets",
            src: [
              "img/**",
              "js/*"
            ]
          }
        ]
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-prettify');
  grunt.loadNpmTasks('grunt-newer');
  grunt.loadNpmTasks('grunt-open');

  // Default task(s).
  grunt.registerTask('default', ['compass', 'jade', "copy:dist"]);
  grunt.registerTask('server', ['compass', 'jade', 'connect:server', 'open', 'watch']);
  grunt.registerTask('html', ['jade']);

};